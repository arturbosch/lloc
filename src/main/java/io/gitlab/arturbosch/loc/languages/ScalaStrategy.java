package io.gitlab.arturbosch.loc.languages;

/**
 * @author artur
 */
class ScalaStrategy extends OtherJVMLanguageStrategy {

	ScalaStrategy() {
		super(Language.SCALA);
	}

}
