package io.gitlab.arturbosch.loc.languages;

/**
 * @author artur
 */
class KotlinStrategy extends OtherJVMLanguageStrategy {

	KotlinStrategy() {
		super(Language.KOTLIN);
	}

}
