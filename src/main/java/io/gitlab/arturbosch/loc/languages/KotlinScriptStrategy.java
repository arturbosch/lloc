package io.gitlab.arturbosch.loc.languages;

/**
 * @author artur
 */
class KotlinScriptStrategy extends OtherJVMLanguageStrategy {

	KotlinScriptStrategy() {
		super(Language.KOTLIN_SCRIPT);
	}

}
