package io.gitlab.arturbosch.loc.languages;

import java.util.List;

/**
 * Abstract class for language strategies. Subclasses have to override the analyze method.
 * <p/>
 * Created by artur on 10.05.15.
 */
public abstract class LanguageStrategy {

	private Language language;

	LanguageStrategy(final Language language) {
		this.language = language;
	}

	/**
	 * @return the language of this language strategy
	 */
	public Language getLanguage() {
		return language;
	}

	/**
	 * Checks if the given filename has the same language of this language strategy.
	 * This uses the simple language check also used for instantiating the language strategy
	 * via the factory. If a language has many endings you can override this method.
	 *
	 * @param filename file to check language
	 * @return true if endings are equal
	 */
	public boolean isLangOfFileSame(String filename) {
		return filename.endsWith(language.getEnding());
	}

	/**
	 * Analyzes the given lines and returns the logical loc.
	 *
	 * @param lines         lines to analyze
	 * @param isCommentMode if lines of comments should be counted too
	 * @param isFullMode    if import and package statements should be counted too
	 * @return loc of the lines
	 */
	public abstract int analyze(List<String> lines, boolean isCommentMode, boolean isFullMode);
}
