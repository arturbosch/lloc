package io.gitlab.arturbosch.loc.languages;

import java.util.Arrays;
import java.util.List;

/**
 * Provides methods to build language strategies.
 * <p>
 * Created by artur on 10.05.15.
 */
public class LanguageStrategyFactory {

	public static final List<Language> LANGUAGES = Arrays.asList(Language.values());

	/**
	 * Builds a language strategy for provided language.
	 *
	 * @param ending language as string
	 * @return a language strategy for given language
	 */
	public static LanguageStrategy getInstance(final String ending) {
		String lang = ending.toLowerCase();
		return LANGUAGES.stream()
				.filter(language -> language.getEnding().equals(lang))
				.findFirst()
				.map(Language::getStrategy)
				.orElse(NullStrategy.INSTANCE);
	}
}
