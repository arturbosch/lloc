package io.gitlab.arturbosch.loc.languages;

import java.util.function.Supplier;

/**
 * @author Artur Bosch
 */
public enum Language {
	JAVA("java", JavaStrategy::new),
	KOTLIN("kt", KotlinStrategy::new),
	KOTLIN_SCRIPT("kts", KotlinScriptStrategy::new),
	SCALA("scala", ScalaStrategy::new),
	GROOVY("groovy", GroovyStrategy::new),
	UNSUPPORTED("NULL", NullStrategy::new);

	private String ending;
	private Supplier<LanguageStrategy> strategy;

	Language(String ending, Supplier<LanguageStrategy> strategy) {
		this.ending = ending;
		this.strategy = strategy;
	}

	public String getEnding() {
		return ending;
	}

	public LanguageStrategy getStrategy() {
		return strategy.get();
	}
}
